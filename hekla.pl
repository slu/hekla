#!/usr/bin/env perl

use 5.12.0;
use warnings;
binmode STDOUT, ":encoding(UTF-8)";

use Math::Cartesian::Product;

my $n = $ARGV[0] || 4;
my @letters = 'a'..'b';
my $ord_offset = ord($letters[0]);
my $axis = "\N{RIGHT ONE EIGHTH BLOCK}";
my $block = "\N{FULL BLOCK}";
my $dat_file = "hekla-${n}.dat";
my @list;

cartesian { push @list, join "", @_; 1} ([@letters]) x $_ for 1..$n;

open my $file, '>', "$dat_file" or die $!;

my $count = 1;
for my $val (map { oct "0b" . join "", map { ord($_) - $ord_offset } split "", $_ } (sort @list)) {
    say $file $count++, ' ', $val;
    say $axis,$block x $val if $n < 8;
}

close $file;

say "Saved dat file in: $dat_file";
