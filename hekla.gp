#!/usr/bin/env gnuplot

set boxwidth 0.5
set style fill solid
unset key

dat_file = sprintf("hekla-%s.dat", ARG1)
png_file = sprintf("hekla-%s.png", ARG1)

set terminal png size 3840, 2160
set output png_file

plot dat_file using 1:2 with boxes

print "Saved bar graph in: ", png_file
