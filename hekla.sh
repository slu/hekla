#!/usr/bin/env bash

set -eu

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

n=${1:-4}

perl "${script_dir}/hekla.pl" "$n"
gnuplot -c ./hekla.gp "$n"
